<?php
use App\Http\Controllers\AuthControllers;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeControllers;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/form',[AuthControllers::class,'get']);

Route::get('/welcome',[HomeControllers::class,'get']);

Route::post('/getName',[AuthControllers::class,'post']);

route::get('/master',function(){
    return view('layout.master');
});

route::get('/table',function(){
    return view('layout.table');
});

route::get('/data',function(){
    return view('layout.data');
});

// Route::post('/coba',function(){
//     $firstname = $request['firstname'];
//     $lastname = $request['lastname'];
//     return view('index',['firstname'=>$firstname,'lastname'=>$lastname]);
// })

