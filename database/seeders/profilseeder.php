<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class profilseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profil')->insert([
            'name' => Str::random(10),
            'alamat' => Str::random(50).'',
            '' => Hash::make(''),
        ]);
    }
}
    
